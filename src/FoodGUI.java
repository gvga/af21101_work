import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton sobaButton;
    private JButton gyouzaButton;
    private JButton syuumaiButton;
    private JButton CheckOutButton;
    private JLabel Total;
    private JLabel Orderditems;
    private JTextArea OrderdItems;
    private JTextArea textBox;
    private  int total = 0;
    private String currentText="";


    //写真よりメニューを選択して注文するかどうかを決める。
    public int Add_Item(int price,String name){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+name+" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0) {
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + name + "! It will be served as soon as possible.");
            total += price;
            Total.setText("Total        " + total + " yen");
            SetTextItem(name + " " + price + "yen");
            return total;
        }
        return total;
    }

    public void SetTextItem(String Item){
        currentText=OrderdItems.getText();
        OrderdItems.setText(currentText+Item+"\n");
    }

        ////合計金額の表示と商品の料金
    public FoodGUI() {

        Total.setText("Total        "+total+" yen");
        tempuraButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(600,"tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(1000,"ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(700,"udon");
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(650,"soba");
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(300,"gyouza");
            }
        });
        syuumaiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {Add_Item(250,"syuumai");
            }
        });

        //料金を確定する
        CheckOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you. The total price is "+total+" yen.");
                    total=0;
                    currentText="";
                    Total.setText("Total        "+total+" yen");
                    OrderdItems.setText(currentText);
                }
            }
        });
    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
